mongodb_driver
=====

## 简介

用erlang编写的Mongodb的驱动程序库，支持主从副本集连接模式；

## 目录

|- include

|	|- mongodb_driver.hrl			%% 数据结构定义文件

|- src

|	|- api

|		|- mc_pool_api.erl				%% 使用进程池方式处理数据库请求和连接

|		|- mc_worker_api.erl			%% 处理数据库请求的基本接口 

|	|- common		%% 数据库处理逻辑目录

|- test

|	|- mc_worker_api_test.erl		%% 测试用例

编译
-----

    $ rebar3 compile

## 使用

### 1. 初始化（只有应用程序启动时调用一次）

```erlang
application:ensure_all_started(mongodb_driver).
```

### 2. 启动连接池和进程处理池

- 单连接方式

  ```erlang
  -define(MONGO_TEST_POOL, 'mongo_test_pool').
  ConnectArgs = 
  	[    
       {host, "127.0.0.1"},    
       {port, 27017},    
       {database, <<"test">>},    
       {user, <<"root">>},    
       {password, <<"">>},    
       {auth_source, <<"admin">>}
      ],
  mc_pool_api:start_pool(?MONGO_TEST_POOL, ConnectArgs).
  ```

- 主从副本集连接方式

  ```erlang
  -define(MONGO_TEST_POOL, 'mongo_test_pool').
  ConnectArgs = 
  	[    
       {host_res, [{"127.0.0.1",32000},{"127.0.0.1",32001},{"127.0.0.1",32002}]},
       {database, <<"test">>},    
       {user, <<"root">>},    
       {password, <<"">>},    
       {auth_source, <<"admin">>}
      ],
  mc_pool_api:start_pool(?MONGO_TEST_POOL, ConnectArgs).
  ```

### 3. 建立索引

- 建立唯一索引

  ```erlang
  mc_pool_api:ensure_index(?MONGO_TEST_POOL, <<"test">>, <<"role">>, {<<"id">>,1}, true)
  ```

- 建立普通索引

  ```erlang
  mc_pool_api:ensure_index(?MONGO_TEST_POOL, <<"test">>, <<"role">>, {<<"name">>,1})
  ```

### 4. 插入

- 插入一条记录

  ```erlang
  Doc = 
  	#{    
        <<"_id">> => 1,    
        <<"id">> => 1,    
        <<"name">> => <<"t01">>,    
        <<"exp">> => 101.0
      },
  {true, ResultDoc} = mc_pool_api:insert(?MONGO_TEST_POOL, <<"test">>, <<"role">>, Doc).
  ```

- 插入多条记录

  ```erlang
  Docs = 
  	[#{
  		<<"_id">> => 2,    
  		<<"id">> => 2,    
  		<<"name">> => <<"t02">>,    
  		<<"exp">> => 102.0
  	},
  	#{   
  		<<"_id">> => 3,    
  		<<"id">> => 3,    
  		<<"name">> => <<"t03">>,    
  		<<"exp">> => 103.0
  	}],
  {true, ResultDoc} = mc_pool_api:insert(?MONGO_TEST_POOL, <<"test">>, <<"role">>, Docs).
  ```

### 5. 删除

- 删除多条记录

  ```erlang
  {true, ResultDoc} = mc_pool_api:delete_all(?MONGO_TEST_POOL, <<"test">>, <<"role">>, #{}).
  ```

- 删除一条记录

  ```erlang
  SelectorDoc = 
  	#{    
        <<"id">> => 1
      },
  {true, ResultDoc} = mc_pool_api:delete_one(?MONGO_TEST_POOL, <<"test">>, <<"role">>, SelectorDoc).
  ```

### 6. 更新

- 只更新存在的第一条记录

  ```erlang
  SelectorDoc = 
  	#{    
        <<"id">> => 1
      },
  UpdateDoc = 
      #{    
        <<"$set">> => 
        	#{       
            <<"name">> => <<"uutest01">>    
          }
      },
  {true, ResultDoc} = mc_pool_api:update(?MONGO_TEST_POOL, <<"test">>, <<"role">>, SelectorDoc, UpdateDoc).
  ```

- 只更新存在的多条记录

  ```erlang
  SelectorDoc = 
  	#{    
        <<"id">> => 1
      },
  UpdateDoc = 
      #{    
        <<"$set">> => 
        	#{        
            <<"name">> => <<"uutest01">>    
          }
      },
  {true, ResultDoc} = mc_pool_api:update(?MONGO_TEST_POOL, <<"test">>, <<"role">>, SelectorDoc, UpdateDoc, false, true).
  ```

- 更新记录，如果记录不存在时自动插入

  ```erlang
  SelectorDoc = 
  	#{    
        <<"id">> => 1
      },
  UpdateDoc = 
      #{    
        <<"$set">> => 
        	#{        
            <<"name">> => <<"uutest01">>    
          }
      },
  {true, ResultDoc} = mc_pool_api:update(?MONGO_TEST_POOL, <<"test">>, <<"role">>, SelectorDoc, UpdateDoc, true, false).
  ```

### 7. 查询

- 查询获取一条记录

  ```erlang
  SelectorDoc = 
  	#{    
        <<"id">> => 1
      },
  case mc_pool_api:find_one(?MONGO_TEST_POOL, <<"test">>, <<"role">>, SelectorDoc) of
      {true, undefined} ->
          %% 没有查询结果
          undefined;
      {true, Doc} ->
          %% 返回查询结果
          Doc;
      {false, ErrorDoc} ->
          %% 报错处理
          undefined
  end.
  ```

- 查询多条记录

  ```erlang
  SelectorDoc =
      #{
        <<"id">> => 
          #{
            <<"$gt">> => 0
           }
      },
  %% 排序字段
  Sortor = 
      #{
        <<"id">> => 1
      },
  Projector = 
      #{
        <<"id">> => 1,
        <<"name">> => 1
      },
  case mc_pool_api:find(?MONGO_TEST_POOL, <<"test">>, <<"role">>, SelectorDoc, Sortor, Projector) of
      {ok, FirstBatchDocs, undefined} ->
          %% 不带游标的情况
          FirstBatchDocs;
      {ok, FirstBatchDocs, CursorPid} ->
          %% 带游标的情况
          %% 读取游标下一个100条记录数据
          case mc_cursor:next(CursorPid, 100) of
          	{ok, []} -> 
                  %% 游标数据全部读取完
                  mc_cursor.close(CursorPid),
     				ok;
          	{ok, NextBatchDocs} ->
              	NextBatchDocs;
          	_ -> ok
      	end;
      {false, ErrorDoc} ->
          %% 报错处理
          undefined
  end.
  ```

  ```erlang
  
  ```
