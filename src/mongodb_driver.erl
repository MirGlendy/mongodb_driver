-module(mongodb_driver).
-behaviour(application).
-author("Gaylen").
-include("mongodb_driver.hrl").

-export([
    start/2,
    stop/1
]).


%% @hidden
start(_, _) ->
    ets:new(?ETS_MONGO_ID, [named_table, public, {write_concurrency, true}, {read_concurrency, true}]),
    ets:insert(?ETS_MONGO_ID, [
        {oid_counter, 0},
        {oid_machineprocid, oid_machineprocid()},
        {requestid_counter, 0}
    ]),
    ok.

%% @hidden
stop(_) ->
    ok.

%% @private
-spec oid_machineprocid() -> <<_:40>>.
oid_machineprocid() ->
    OSPid = list_to_integer(os:getpid()),
    {ok, Hostname} = inet:gethostname(),
    <<MachineId:3/binary, _/binary>> = erlang:md5(Hostname),
    <<MachineId:3/binary, OSPid:16/big>>.

