%%--- coding:utf-8 ---
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% File Name: mc_pool_logic
%%% Created on : 2024/9/5 22:24
%%% @author Gaylen 252323463@qq.com
%%% @copyright (C) 2024, freedom
%%% @doc
%%%
%%% @end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-module(mc_pool_logic).
-author("Gaylen").
-include("mongodb_driver.hrl").

%% API
-export([
    init/0,
    sync_command/5,
    sync_insert/5,
    sync_update/8,
    sync_delete/6,
    sync_find_one/6,
    sync_find/10,
    sync_find_and_modify/8,
    sync_count/7,
    sync_ensure_index/7
]).

init() ->
    erlang:process_flag(trap_exit, true),
    #mongo_pool_state{}.

sync_command(State, WorkerPid, Database, Command, IsSlaveOk) ->
    Result = mc_worker_api:command(WorkerPid, Database, Command, IsSlaveOk),
    {ok, State, Result}.

sync_insert(State, WorkerPid, Database, Table, Docs) ->
    Result = mc_worker_api:insert(WorkerPid, Database, Table, Docs),
    {ok, State, Result}.

sync_update(State, WorkerPid, Database, Table, Selector, Doc, IsUpInsert, IsMultiUpdate) ->
    Result = mc_worker_api:update(WorkerPid, Database, Table, Selector, Doc, IsUpInsert, IsMultiUpdate),
    {ok, State, Result}.

sync_delete(State, WorkerPid, Database, Table, Selector, Limit) ->
    Result = mc_worker_api:delete_limit(WorkerPid, Database, Table, Selector, Limit),
    {ok, State, Result}.

sync_find_one(State, WorkerPid, Database, Table, Selector, Projector) ->
    Result = mc_worker_api:find_one(WorkerPid, Database, Table, Selector, Projector),
    {ok, State, Result}.

sync_find(State, WorkerPid, Database, Table, Selector, SortDoc, Projector, Skip, Limit, IsSlave) ->
    Result = mc_worker_api:find(WorkerPid, Database, Table, Selector, SortDoc, Projector, Skip, Limit, IsSlave),
    {ok, State, Result}.

sync_find_and_modify(State, WorkerPid, Database, Table, Selector, UpdateDoc, Projector, IsUpInsert) ->
    Result = mc_worker_api:find_and_modify(WorkerPid, Database, Table, Selector, UpdateDoc, Projector, IsUpInsert),
    {ok, State, Result}.

sync_count(State, WorkerPid, Database, Table, Selector, Limit, Skip) ->
    Result = mc_worker_api:count(WorkerPid, Database, Table, Selector, Limit, Skip),
    {ok, State, Result}.

sync_ensure_index(State, WorkerPid, Database, Table, IndexSpecs, Unique, DropDups) ->
    Result = mc_worker_api:ensure_index(WorkerPid, Database, Table, IndexSpecs, Unique, DropDups),
    {ok, State, Result}.

